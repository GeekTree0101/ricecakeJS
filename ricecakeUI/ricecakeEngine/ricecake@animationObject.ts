
export class RicecakeObject{
    
    private tool :any;
    
    constructor(){
        
        this.tool = 
        
        {
            "show-up" : {
                "framework" : [
                    {opacity :0},
                    {opacity :1}
                ],
            
                "easing" : 'ease-in-out',
                "iterations" : 1,
                "direction" : 'alternate',
                "fill" : 'none'
            
            }
        }
    }
    
    
    protected retAnimationTool(stringValue: any){
        
        let temp = this.tool[stringValue];
        
        console.log("[+] return animation object from retAnimationTool");
        
        return {
            framework : temp["framework"],
            easing : temp["easing"],
            iteraction : temp["iteractions"],
            direction : temp["direction"],
            fill : temp["fill"]
        };
    }
    
    
}