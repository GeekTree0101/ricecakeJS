import {RicecakeObject} from "./ricecake@animationObject";


export class ricecake_animation_engine extends RicecakeObject{


   makeAnimation(animationObject : any){
       
       let timeDelay = animationObject.delay;
       let timeDuration = animationObject.duration;
       
       if(typeof(animationObject.action) == "string"){
            
            console.log("[+] ricecake animation engine act");
            let animation = super.retAnimationTool(animationObject.action);
       
            return [animation.framework,{
           
                duration : timeDuration,
                easing : animation.easing,
                delay: timeDelay,
                iterations : animation.iteraction, //1
                direction : animation.direction,   //alternate
                fill : animation.fill              //forwards
            }];
       }
       else{
           
           timeDelay = null;
           timeDuration = null;
           return [animationObject.action[0],animationObject.action[1]];
           
       }
   }

    
}


