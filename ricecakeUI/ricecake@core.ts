/// <reference path="./../typings/index.d.ts" />
/**
 *  RiceCake BootStrap section
 *  provider : GeekTree0101(Hyeonsu Ha)
 *  update : 2016.08.14
 */
import {ricecake_animation_engine} from "./ricecakeEngine/ricecake@animationEngine";

class RiceCake{

    private init_processor_main_application : any;

    constructor(init : any){
        
        console.log(`RiceCake User Interface Project \n
        version : In-Jeol-Mi (1.0.1) \n
        update : 2016.08.14 \n
        provider : GeekTree0101(Hyeonsu Ha) \n
        Copyright (c) MIRO interaction team \n
        `);
        
        this.init_processor_main_application = new init;
        
    }
    
}

class ricecake_Core_application{
    
    
    /** 
     *  [CAUTION!]
     *  <DO NOT TOUCH THIS VARIABLE>
     */
    protected presentView : any;
    protected previousView : any;
    protected ViewList : any;
    
    protected make_element_child_node(target_id : string, element_value : string){
        /**
         *  make_element_child_node
         *  : production child node such as virtual element
         * 
         *  @param {
         * 
         *     target_id : new tag name
         *     element_value : innerHTML on new tag named eleemnt
         *  }
         */
        return "<" + target_id + ">" + element_value + "</" + target_id + ">";
    }
    
    protected append_view_list(target_name : string, node_element: any){
         
         /**
         *   append_view_list
         *   : append made new element on target element  
         *   
         *   @param {
         *       
         *      target_name : target node
         *      node_element : it will append on target node
         *   }
         */
        
        if(this.ViewList == undefined){
            
            console.log("ViewList is undefined!");
            this.ViewList = {};
        }
        
        let temp_data = {
            "name" : target_name,
            "element" : node_element
        }
        
        this.ViewList[target_name] = temp_data;         //caching to list
        
    }
    
}


export class createView extends ricecake_Core_application{
         

    private view_cached_memory_optimization = 100;         //limit View element counts
    
    appendView(target : any, element_value : string){
         
         /**
         *   appendView
         *   : push new view element with caching on ricecake-init tree
         *   
         *   @param {
         *       
         *      target : view elements , type : any
         *      element_value : make new tag name <element_value></element_value>
         *   }
         */       
        
        //TODO: append Node to ricecake-init Dom tree
        
        setTimeout(()=>{
            //Lazy loading.    
            console.log("[+] instance",target.name);
            new target;
        },1000);
        
        
        let temp_name = target.name;
        let temp_node = super.make_element_child_node(temp_name, element_value);
         
        $("ricecake-init").append(temp_node);
        $(temp_name).hide(0);
        
        super.append_view_list(temp_name, $(temp_name));
        
        temp_name = null;
        temp_node = null;
    }
    
    pushView(target : any){
        
        /**
         *   pushView
         *   : push View element and show next View to user on display
         *   
         *   @param {
         *       
         *      target : view elements , type : any
         *   }
         */
  
        let temp_previous = this.previousView;
        
        if(!temp_previous){
            
            //console.log("[-] empty previousView list");
            temp_previous = [];                                //create void list
            temp_previous.push(target.name);
            
            this.previousView = temp_previous;
        }
        else{
            //console.log("[+] append previousView", temp_previous);
            
            this.ViewList[temp_previous[temp_previous.length - 1]].element.hide(0);
            
            temp_previous.push(target.name);
            this.previousView = temp_previous;
        }
  
        this.presentView = target.name;
        this.ViewList[target.name].element.show(0);
        this.autoRemove();                                 //previous View autoRemove for memory optimization
    }
    
    popView(){
        
         /**
         *   popVie
         *   : push Before cached view to user on display
         *   
         */       
        
        //console.log("[+] back before View");
        let temp_present = this.presentView;
        let temp_previous = this.previousView;
        let temp_cached_view = this.ViewList[temp_previous[temp_previous.length - 2]];   //before view
        
        //console.log("[*]before:previous",temp_previous);
        
        this.ViewList[temp_present].element.hide(0);      //hiding previous view
        temp_cached_view.element.show(0);                 //show before view
        
        
        this.previousView.pop();                         //remove present view
        this.presentView = temp_cached_view.name;        //caching before view
        
        //console.log("[*]reload:previous",this.previousView);
        
        temp_present = null;
        temp_previous = null;
        
        return temp_cached_view.element;
       
    }
    
    private autoRemove(){
        
        let temp = this.previousView;
        
        if(temp.length > this.view_cached_memory_optimization){
            
            this.previousView.shift();                  //using Queue structure
        }
        
        temp = null;
        
    }
    
    option(view_cached_memory_optimization_setting : number){
        
        //TODO : need more various options
        this.view_cached_memory_optimization = view_cached_memory_optimization_setting;
        
    }
    
}


export class Interface extends ricecake_Core_application{
    
    private Engine : any;
    private nodeList : any;
    private timeline : any;

    
    createDOM(objectList : any){
        /**
         *  createDOM
         *  :append new created DOM
         * 
         *  @param {
         * 
         *     objectList : type is List []
         *     {
         *        target : parant node / type : string
         *        name   : make new node name / type : string
         *        element : new node inner value / type :string
         *        state : true/false : true is active state , false is disable state
         *        animation : {
         *        
         *            delay : number,    [sec]
         *            duration : number, [sec]
         *            action : string or USER_CUSTOM_ANIMATION [framework,option]
         *        }
         *     }
         *  }
         */
         if(this.Engine == undefined){
            
            console.log("[+] Interface create");    
            this.Engine = new ricecake_animation_engine();
            this.nodeList = {};             
         }
         
         
         console.log("[+] create Dom");
         
         for(const index in objectList){
             
             let temp_object = objectList[index];
             let temp_new_created_node = super.make_element_child_node( temp_object.name, temp_object.element );
             let temp_set_animation = this.Engine.makeAnimation(temp_object.animation);
              
             $(temp_object.target).append(temp_new_created_node);
             
             let dom = (<any>window).document.querySelector(temp_object.name);
             
             dom = dom.animate(temp_set_animation[0],temp_set_animation[1]);
             
             
             if(temp_object.state){
                 dom.pause();                   //일시정지만
             }
             else{
                 dom.pause();                   //일시정지 및 숨김
                 $(temp_object.name).hide(0);
             }
             
             
             this.nodeList[temp_object.name] = {
                 state : temp_object.state,
                 node : dom
             }
             
             temp_object = null;
             temp_new_created_node = null;
             temp_set_animation = null;
             
         }
        
        
    }
    
    timeliner(arrayList : any){
        
        this.timeline = arrayList;    
    }
    
    act(target : any){
        
        let temp = this.timeline;
        let temp_node_list = this.nodeList;
            
            if(target == "Timeline"){
            
                for(const index in temp){
                
                    if(!temp_node_list.temp[index].state){
                        $(temp[index]).show(0);
                        this.nodeList.temp[index].state = true;
                    }
                
                    temp_node_list.temp[index].node.play();
                
                }    
            }
            else{
                
                    if(!temp_node_list[target].state){
                        $(target).show(0);
                        this.nodeList.target.state = true;
                    }
                       
                    console.log("DEBUG",temp_node_list[target]);
                    temp_node_list[target].node.play();        
            }
            
      temp = null;
      temp_node_list = null;      
   
   }

}
export function ricecakeBootStrap(appStartPoint : any){
    /**
     * 
     *  RiceCake UI Bootstrap
     * 
     */
    $("body").append("<ricecake-init></ricecake-init>");
    
    new RiceCake(appStartPoint);
}

