/**
 * 
 *    Ricecake Gulp Build system
 * 
 *    provider : Ha Hyeonsu
 *    update : 2016.08.14
 * 
 *    copyright(c) 2016 MIRO interation team
 */


/**
 *  Import Gulp module
 * 
 */
var gulp = require("gulp"),
    sassBuilder = require("gulp-sass"),          
    run = require("gulp-exec"),                 
    runSequence = require("run-sequence"),     
    watch = require("gulp-watch"),               
    remove = require("del"),
    copy = require("gulp-copy"),
    connect = require("gulp-connect"),
    livereload = require('gulp-livereload');                     
 

/**
 *  Group Task Section
 */
 
gulp.task("[ricecake]:webDebug", function(){               
    
   return runSequence("[ricecake]:Build","[react:libCopy]","[ricecake]:livereload","[ricecake]:server","[ricecake]:watch"); 

});

gulp.task("[ricecake]:livereload", function(){             //Before you must install google chrome livereload
    
    return gulp.src(["*","./app/*","./www/*"])
               .pipe(livereload());
    
});

gulp.task("[ricecake]:Build",function(){
    
    return runSequence(["ricecakeStylesheet","ricecakeComplie"],"ricecakePacking");
});

 /**
  *   Watch Task Section
  */
 
 gulp.task("[ricecake]:watch", function(){
     
      livereload.listen();
      gulp.watch(["./app/**/*.ts"],["[ricecake:livereload]:compile","[ricecake]:livereload"]);
      gulp.watch(["./app/**/*.scss"],["[ricecake:livereload]:sass","[ricecake]:livereload"]);
      
      //dev
      gulp.watch(["./ricecakeUI/**/*.ts"],["[ricecake:livereload]:compile","[ricecake]:livereload"]);
      gulp.watch(["./ricecakeUI/**/*.scss"],["[ricecake:livereload]:sass","[ricecake]:livereload"]);     
 })

 
 gulp.task("[ricecake:livereload]:sass",function(){
     
     return runSequence(["ricecakeStylesheet"],["[ricecake]:livereload"]);
 });
 
 gulp.task("[ricecake:livereload]:compile",function(){
     
     return runSequence(["ricecakeComplie"],["ricecakePacking"],["[ricecake]:livereload"]);
 });

 gulp.task("[react:libCopy]",function(){
     return runSequence("ReactCopy","ReactDomCopy");
 })   
/**
 *  Unit Task Section 
 */  

gulp.task("[ricecake]:server",function(){
    
    connect.server({
        root: "./",
        port: 8888
    });
    
})  

gulp.task("ReactCopy", function(done){
    return gulp.src("./node_modules/react/dist/react.js")
               .pipe(copy("./www/js"));
});

gulp.task("ReactDomCopy", function(done){
     return gulp.src("./node_modules/react-dom/dist/react-dom.js")
               .pipe(copy("./www/js"));   
});  
  
gulp.task("ricecakeStylesheet", function(done){     
    
    return gulp.src("./app/ricecake.core.scss")
        .pipe(sassBuilder({base : "./"}))
        .pipe(gulp.dest(function(file){
            return "./www/css";             
        }))

})

gulp.task("ricecakeComplie", function(done){      

    return gulp.src("./")
               .pipe(run("sudo tsc -p ./"));
})


gulp.task("ricecakePacking", function(done){        
    
    return gulp.src("./temp")
               .pipe(run("webpack"));
})


gulp.task("cleanApp", function(done){     
    
    remove("./temp/*");
 
    remove("./www/js/*");
    remove("./www/css/*");
    
    return "done";
})

