Welcome to ricecakeUI
=====================

![ricecake!](./ricecakeUI/ricecakeResource/splash.png)
### Chewing Design, Sweet User Interface
> #### HTML5 Hybrid Mobile Application framework </h4>
>> ##### Webpack https://webpack.github.io/
>> ##### Cordova https://cordova.apache.org/
>> ##### Jquery https://jquery.com/


How to Use it?
--------------
### 1. install environment
> #### npm install -d
> #### typings install
> #### cordova platform add android // you can ios too


### 2. develop
> #### gulp [ricecake]:webDebug ---> localhost:8888/www

### 3. build
> #### cordova build android
> #### [or] cordova run android // must connect your mobile or emulator
