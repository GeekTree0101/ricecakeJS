import {ricecakeBootStrap, createView} from "./../ricecakeUI/ricecake@core.ts";
import {aboutApplication} from "./pages/about/about";
import {homeApplication} from "./pages/home/home";
import {nfcApplication} from "./pages/nfc/nfc";

export class main{
        
    private appRoot : any;
    private view : any;
        
    private view_element = {
        "homeApplication" : `
            <h1> homeApplication </h1>
            <button id="b2"> next </button>
        `,
        "aboutApplication" : `
            <h1> aboutApplication </h1>
            <button id="b3"> next </button>
            <hr/>
            <button id="back1"> back </button>
        `,
        "nfcApplication" : `
            <h1> nfcApplication </h1>
            <button id="b1"> home </button>
            <hr />
            <button id="back2"> back </button>
        `
    }    
        
    constructor(){
        
        this.view = new createView;
        
        this.view.appendView(homeApplication,this.view_element["homeApplication"]);
        this.view.appendView(nfcApplication,this.view_element["nfcApplication"]);
        this.view.appendView(aboutApplication,this.view_element["aboutApplication"]);
        
        this.view.option(3);
        
        this.appRoot = homeApplication;
        
        this.view.pushView(this.appRoot);   
        
        console.log("[+] Main start");
        
        $("#b1").click(()=>{
            this.appRoot = homeApplication;
            this.view.pushView(this.appRoot); 
        });
        $("#b2").click(()=>{
            this.appRoot = aboutApplication;
            this.view.pushView(this.appRoot); 
        });
        $("#b3").click(()=>{
            this.appRoot = nfcApplication;
            this.view.pushView(this.appRoot); 
        });
        $("#back1").click(()=>{
            console.log("user pressed back button");
            this.appRoot = this.view.popView();
        })
        $("#back2").click(()=>{
            console.log("user pressed back button");
            this.appRoot = this.view.popView();
        })
    }
 
    
}

ricecakeBootStrap(main);