import {Interface} from "./../../../ricecakeUI/ricecake@core";


export class homeApplication{
    
    private ricecakeInterface : any;
    
    private elements = [
        {
            target : "homeapplication",
            name : "new-button",
            element : `
                <button id="bt"> newButton </button>
            `,
            state : true,
            animation : {
                delay : 0,
                duration : 2000,
                action : 'show-up'
            }
        }
        
    ]
    
    constructor(){
        
        console.log("[+] homeApplication start");
        
        this.ricecakeInterface = new Interface;
        this.ricecakeInterface.createDOM(this.elements);
        
        $("#bt").click(() => {
            this.run();
        });
        
    }
    
    private run(){
        
        console.log("[+] act");
        this.ricecakeInterface.act("new-button");
    }
    
    
    
}